﻿using Expertel.Challenge;
using Expertel.Persistence;

namespace ExpertelChallenge
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var executor = new ChallengeExecutor(new ExpertelContext());
            executor.RunChallenge();
        }

    }
}