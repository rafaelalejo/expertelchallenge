﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expertel.Domain.Entities
{
    public class Meeting
    {
        public Meeting(int userId, string name, DateTime startTime, DateTime endTime)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            UserId = userId;
            StartTime = startTime;
            EndTime = endTime;
        }

        // ORM workaround
        private Meeting() { }

        protected DateTime _endTime;

        public int Id { get; protected set; }

        public int UserId { get; private set; }

        public string Name { get; set; }

        public DateTime StartTime { get; protected set; }

        public DateTime EndTime
        {
            get
            {
                return _endTime;
            }

            protected set
            {
                if (value <= StartTime)
                    throw new ArgumentOutOfRangeException("Invalid end time.");
                _endTime = value;
            }
        }
    }
}
