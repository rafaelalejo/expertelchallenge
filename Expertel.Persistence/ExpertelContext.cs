﻿using Expertel.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Expertel.Persistence
{
    public class ExpertelContext  : DbContext
    {

        public DbSet<Meeting> Meetings { get; set; }

        protected string DbPath { get; }

        public ExpertelContext()
        {
            var path = Environment.CurrentDirectory;
            DbPath = Path.Join(path, "blogging.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Apply mappings in the same .dll
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

    }
}