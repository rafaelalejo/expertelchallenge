﻿using Expertel.Domain.Entities;
using Expertel.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Expertel.Challenge
{
    public class ChallengeExecutor
    {
        private readonly ExpertelContext dbContext;

        public ChallengeExecutor(ExpertelContext dbContext)
        {
            this.dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public void RunChallenge()
        {
            createTable();

            // Initial meeting
            scheduleMeeting("Status Meeting", DateTime.Parse("2022-09-27 09:00:00"), DateTime.Parse("2022-09-27 10:00:00"), new int[]{1,2,3});

            // User 3 should have a conflict with status meeting
            scheduleMeeting("Peer Review", DateTime.Parse("2022-09-27 09:30:00"), DateTime.Parse("2022-09-27 10:00:00"), new int[]{3, 4});

            // All users should have conflicts
            // Conflicts with the status meeting - Meeting overlaps
            scheduleMeeting("Peer Review 2", DateTime.Parse("2022-09-27 09:30:00"), DateTime.Parse("2022-09-27 10:30:00"), new int[]{1,2,3});

            // Conflicts with the status meeting - Meeting inside another meeting 
            scheduleMeeting("Peer Review 3", DateTime.Parse("2022-09-27 09:45:00"), DateTime.Parse("2022-09-27 10:50:00"), new int[]{1,2,3});

            // There should be no conflicts in this meeting
            scheduleMeeting("Peer Review 4", DateTime.Parse("2022-09-27 09:30:00"), DateTime.Parse("2022-09-27 10:30:00"), new int[]{4});

            // There should be no conflicts in this meeting - starts right after Peer Review 4
            // Meeting that spans for more than 1 day
            scheduleMeeting("Peer Review 5", DateTime.Parse("2022-09-27 10:30:00"), DateTime.Parse("2022-09-28 10:00:00"), new int[]{4});

            // User 3 and 4 shouldn't have a conflict - starts right after Peer Review 5
            // Meeting that spans for more than 1 day
            scheduleMeeting("Peer Review 6", DateTime.Parse("2022-09-28 10:00:00"), DateTime.Parse("2022-09-29 11:00:00"), new int[]{3, 4});
        }

        protected void createTable()
        {
            dbContext.Database.ExecuteSqlRaw("DROP TABLE IF EXISTS meetings;");

            dbContext.Database.ExecuteSqlRaw(
@"CREATE TABLE meetings (
  id INTEGER PRIMARY KEY,
  user_id INT NOT NULL,
  start_time DATETIME NOT NULL,
  end_time DATETIME NOT NULL,
  meeting_name VARCHAR(100)
);"
            );
        }

        protected bool scheduleMeeting(string meetingName, DateTime startDateTime, DateTime endDateTime, int[] userIDs)
        {
            // Detect overlaping time
            var conflictingMeetings = dbContext.Meetings
                .Where(m => userIDs.Contains(m.UserId) && m.StartTime < endDateTime && startDateTime < m.EndTime)
                .Select(m => new { m.UserId, m.Name})
                .AsNoTracking()
                .ToList();

            Console.WriteLine($"Trying to schedule meeting {meetingName}, from {startDateTime} to {endDateTime}");

            if (conflictingMeetings.Any())
            {
                foreach (var conflict in conflictingMeetings)
                {
                    Console.WriteLine($"User {conflict.UserId} has a conflicting meeting: {conflict.Name}");
                }

                Console.WriteLine("The meeting has not been booked.");
                return false;
            }

            foreach(var userId in userIDs)
            {
                var meeting = new Meeting(userId, meetingName, startDateTime, endDateTime);
                dbContext.Meetings.Add(meeting);
            }

            dbContext.SaveChanges();

            Console.WriteLine("The meeting has been successfully booked.");

            return true;
        }
    }
}
