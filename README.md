# Introduction

This is my solution for the provided Expertel Challenge.

## Technologies used
- .NET Core 6.0
- Entity Framework Core 6.0
- SQLite EF Driver

# Running this project 
You can run this project by running the following commands in the root directory.
This will restore the project dependencies and run the challenge using a SQLite database.

```
dotnet restore
dotnet run --project Expertel.Challenge
```