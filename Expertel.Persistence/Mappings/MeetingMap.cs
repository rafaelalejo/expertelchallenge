﻿using Expertel.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Expertel.Persistence.Mappings
{
    public class MeetingMap : IEntityTypeConfiguration<Meeting>
    {
        public void Configure(EntityTypeBuilder<Meeting> builder)
        {
            // Key properties
            builder.ToTable("meetings");
            builder.HasKey(c => c.Id);

            // Fields
            builder.Property(c => c.Id)
                .ValueGeneratedOnAdd()
                .IsRequired()
                .HasColumnName("id");

            builder.Property(c => c.Name)
                .IsRequired()
                .HasColumnName("meeting_name");

            builder.Property(c => c.UserId)
                .IsRequired()
                .HasColumnName("user_id");

            builder.Property(c => c.StartTime)
                .IsRequired()
                .HasColumnName("start_time");

            builder.Property(c => c.EndTime)
                .HasField("_endTime")
                .IsRequired()
                .HasColumnName("end_time");
        }
    }
}
